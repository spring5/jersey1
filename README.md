# Getting Started (Gradle)


## Scaffold

    spring init --name=Jersey1 -g=spring5 -a=jersey1 -d=jersey,devtools,actuator --boot=2.0.0.BUILD-SNAPSHOT --build=gradle  spring5-jersey1
    spring init --name=Jersey1Client -g=spring5 -a=jersey1.client -d=spring-shell,jersey,devtools --boot=2.0.0.BUILD-SNAPSHOT --build=gradle  spring5-jersey1-client



## Build

    ./gradlew clean build



## Run

    ./gradlew bootRun






