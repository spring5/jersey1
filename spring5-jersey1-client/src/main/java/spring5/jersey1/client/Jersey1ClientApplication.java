package spring5.jersey1.client;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Jersey1ClientApplication {

	public static void main(String[] args) {
		SpringApplication.run(Jersey1ClientApplication.class, args);
	}
}
